package com.tsystems.javaschool.client.ejb.services.implementation;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.websocket.Session;

import java.util.Set;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class WebSocketServiceImplTest extends TestCase {
    private WebSocketServiceImpl webSocketService;

    @Mock
    Set<Session> clients;

    @Mock
    Session session;

    @Before
    public void setUp() throws Exception {
        webSocketService = new WebSocketServiceImpl();
        webSocketService.setClients(clients);
        clients.add(session);
    }

    @Test
    public void testOnOpen() throws Exception {
        webSocketService.onOpen(session);
        verify(clients, times(2)).add(session);
    }

    @Test
    public void testOnClose() throws Exception {
        webSocketService.onClose(session);
        verify(clients).remove(session);
    }
}