package com.tsystems.javaschool.client.managedBeans;

import com.tsystems.javaschool.client.ejb.DataBean;
import com.tsystems.javaschool.client.model.Product;
import com.tsystems.javaschool.client.model.ProductDTO;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import java.util.List;

/**
 * Managed bean used for JSF access to data
 */
@ManagedBean(name = "statsBean", eager = true)
public class StatsBean {
    @EJB
    private DataBean dataBean;

    private List<ProductDTO> topProducts;

    public StatsBean() {}

    /**
     * initialize bean with data
     */
    @PostConstruct
    private void init() {
        topProducts = dataBean.getTopProducts();
    }

    public List<ProductDTO> getTopProducts() {
        topProducts = dataBean.getTopProducts();
        return topProducts;
    }

    public void setTopProducts(List<ProductDTO> topProduct) {
        this.topProducts = topProduct;
    }
}
