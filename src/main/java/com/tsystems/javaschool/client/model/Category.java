package com.tsystems.javaschool.client.model;

/**
 * Category of product
 */
public class Category {
    private int categoryId;
    private String categoryName;
    private String filterFile;

    @Override
    public String toString() {
        return categoryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String category) {
        this.categoryName = category;
    }

    public String getFilterFile() {
        return filterFile;
    }

    public void setFilterFile(String filterFile) {
        this.filterFile = filterFile;
    }
}
