package com.tsystems.javaschool.client.model;

/**
 * Manufacturer of product
 */
public class Manufacturer {
    private int manufacturerId;
    private String manufacturerName;

    @Override
    public String toString() {
        return manufacturerName;
    }

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }
}
