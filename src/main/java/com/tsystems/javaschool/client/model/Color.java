package com.tsystems.javaschool.client.model;

/**
 * Color of product
 */
public class Color {
    private int colorId;
    private String colorName;

    @Override
    public String toString() {
        return colorName;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }
}
