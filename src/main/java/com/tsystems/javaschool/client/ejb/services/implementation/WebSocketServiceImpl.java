package com.tsystems.javaschool.client.ejb.services.implementation;

import com.tsystems.javaschool.client.ejb.services.api.WebSocketService;
import org.apache.log4j.Logger;

import javax.inject.Named;
import javax.websocket.OnClose;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Named(value = "WebSocketService")
@ServerEndpoint("/websocket/restClient")
public class WebSocketServiceImpl implements WebSocketService {
    private static final Logger log = Logger.getLogger(WebSocketServiceImpl.class);
    private static Set<Session> clients = Collections.synchronizedSet(new HashSet<>());

    @OnOpen
    public void onOpen(Session session) {
        log.info("New websocket session opened: " + session.getId());
        clients.add(session);
    }

    @OnClose
    public void onClose(Session session) {
        log.info("Websocket session closed: " + session.getId());
        clients.remove(session);
    }

    @Override
    public void forceClientsUpdate() {
        log.info("forcing clients to update");
        for (Session s : clients) {
            try {
                s.getBasicRemote().sendText("update");
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }

    public static void setClients(Set<Session> clients) {
        WebSocketServiceImpl.clients = clients;
    }
}
