package com.tsystems.javaschool.client.ejb.services.api;

import javax.jws.WebService;

/**
 * service to interact with websocket clients
 */
@WebService
public interface WebSocketService {
    /**
     * make all connected websocket clients to refresh their pages
     */
    void forceClientsUpdate();
}
