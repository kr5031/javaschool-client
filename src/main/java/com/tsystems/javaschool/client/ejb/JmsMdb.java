package com.tsystems.javaschool.client.ejb;

import com.tsystems.javaschool.client.ejb.services.api.WebSocketService;
import org.apache.log4j.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/**
 * Message driven bean to interact with a Message queue
 * Used to receive notifications about statistics change and
 */
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/topic/eShop"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
        @ActivationConfigProperty(propertyName = "user", propertyValue = "quickstartUser"),
        @ActivationConfigProperty(propertyName = "password", propertyValue = "quickstartPwd1!")}, name = "JmsMdb")
public class JmsMdb implements MessageListener {
    private static final Logger log = Logger.getLogger(JmsMdb.class);

    @Inject
    WebSocketService webSocketService;

    @EJB
    DataBean dataBean;

    @Override
    public void onMessage(Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;
            log.info("got update in the topic: " + textMessage.getText());
            dataBean.refreshData();
            webSocketService.forceClientsUpdate();
        } catch (JMSException e) {
            log.error(e.getMessage());
        }
    }
}
