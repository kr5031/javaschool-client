package com.tsystems.javaschool.client.ejb.services.api;

import com.tsystems.javaschool.client.model.ProductDTO;

import java.util.List;

/**
 * service for interacting with REST clients
 */
public interface RestClientService {
    /**
     * Get top products (by number of pieces sold)
     * @param n numbr of top products
     * @return list of products DTO with included images within
     */
    List<ProductDTO> getTopProducts(int n);
}
