package com.tsystems.javaschool.client.ejb.services.implementation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.util.Base64;
import com.tsystems.javaschool.client.ejb.services.api.RestClientService;
import com.tsystems.javaschool.client.model.ProductDTO;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Stateless
public class RestClientServiceImpl implements RestClientService {
    private static final Logger log = Logger.getLogger(RestClientServiceImpl.class.getName());

    @Override
    public List<ProductDTO> getTopProducts(int n) {
        ProductDTO[] result;
        Client client = Client.create();
        WebResource webResource = client.resource("/rest/prod/" + n);
        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class);
        if (response.getStatus() != 200) {
            System.out.println("getTopProducts() failed with error code " + response.getStatus());
            log.error("getTopProducts() failed with error code " + response.getStatus());
        }
        String output = response.getEntity(String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            result = objectMapper.readValue(output, ProductDTO[].class);
            for (int i = 0; i < result.length; i++) {
                byte[] image = result[i].getImage();
                String base64 = Base64.getEncoder().encodeToString(image);
                result[i].setBase64Image(base64);
                result[i].setImage(null);
            }
            return Arrays.asList(result);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            log.error(e.getMessage());
            return new ArrayList<>();
        }
    }
}
