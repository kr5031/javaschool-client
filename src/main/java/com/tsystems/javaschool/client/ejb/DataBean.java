package com.tsystems.javaschool.client.ejb;

import com.tsystems.javaschool.client.ejb.services.api.RestClientService;
import com.tsystems.javaschool.client.model.Product;
import com.tsystems.javaschool.client.model.ProductDTO;
import org.apache.log4j.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import java.util.List;

/**
 * Bean used for storing data, received via REST
 */
@Singleton
public class DataBean {
    private static final Logger log = Logger.getLogger(DataBean.class);

    @EJB
    private RestClientService restClientService;
    private List<ProductDTO> topProducts;

    public DataBean() {  }

    /**
     * initialize bean with data
     */
    @PostConstruct
    private void init() {
        log.info("DataBean created");
        topProducts = restClientService.getTopProducts(10);
    }

    /**
     * get updated data via REST
     */
    void refreshData() {
        topProducts = restClientService.getTopProducts(10);
    }

    /**
     * get the current top products
     * @return the current top products
     */
    public List<ProductDTO> getTopProducts() {
        refreshData();
        return topProducts;
    }
}
