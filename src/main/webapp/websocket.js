var websocket = null;
var wsURI = 'ws://127.0.0.1:8080/client/websocket/restClient';
websocket = new WebSocket(wsURI);

websocket.onmessage = function (event) {
    if (event.data === 'update') {
       location.reload();
    }
};

function disconnect() {
    if (websocket !== null) {
        websocket.close();
        websocket = null;
    }
}

function sendMessage() {
    if (websocket !== null) {
        websocket.send('message from client');
    } else {
        alert('WebSocket connection is not established. Please click the Open Connection button.');
    }
}